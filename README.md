# Automail.sh

## Description
automail.sh is a mail archiving tool. It can be used with a config file to check different domains and mailboxes against individual archiving schedules and max sizes.

## Installation
Clone the repository.
Move the manfiles from man to /usr/local/man
Either add src to your PATH variable, or copy the scripts from src into a place specified by the PATH variable.

## Command Summary
automail.sh [options {-q | -v} {-u & -d}]

Flags:

    -a  absolute_date  
    -c  path_to_config_file
    -D  mail_directory
    -d  specific_domain_to_use
    -i  absolute_path_to_specific_mailboxes_to_archive(/directory/domain/user)
    -m  override_config_or_default_max
    -q  quiet(suppress_output)
    -s  search_modifier(before, since)  search_date(-x/-x/-x || -a x/x/x)
    -t  archive_type( [Y]early, [H]alf_yearly, [Q]uarterly, [M]onthly, [W]eekly )
    -u  specific_user_to_archive
    -v  verbose

## Config File
The config file follows a simple pattern. The first line is the directory which contains the domain folders. The second line specifies the default max. From there it follows the pattern:

    domain
    user
    schedule[W,M,Q,H,Y]
    max
    search_criteria{ . | modifier date_offset}

. represents "Use whatever was previously specified" which allows you to build up default patterns for domains and users, only specifying where individual domains or users differ from the defaults.

Also '#' is used for commenting.