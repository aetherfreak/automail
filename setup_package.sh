#!/bin/sh

if [ "$#" -ne "1" ]
then
	echo "Usage: $0 version-number"
	exit
fi

version="$1"
directory="automail-$version"

mkdir -p "$directory/src"
cp "src/mail-lib.sh" "src/util-lib.sh" "src/date-lib.sh" "src/setup-lib.sh" "src/automail" "$directory/src"
cp -r man "$directory"

tar -cf "release/tarball/$directory"".tar.gz" "$directory"
