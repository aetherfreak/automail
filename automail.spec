Name:           automail
Version:        1.7
Release:        2%{?dist}
Summary:        Auto archiving mailbox tool based upon schedules for each domain and mailbox

License:        NONE
URL:            https://bitbucket.org/aetherfreak/automail/
Source0:        https://bitbucket.org/aetherfreak/automail/raw/3e0ee64911535e3d830c6e82f15a0441596802cc/release/tarball/automail-1.7.tar.gz

Requires:       bash
#Requires:       mailutil

BuildArch: noarch

%description
Auto archiving mailbox tool based upon schedules for each domain and mailbox

%prep
%setup -q #unpack tarball and place in BUILD folder

%install
mkdir -p %{buildroot}/%{_bindir}
install -m 0755 src/%{name} %{buildroot}/%{_bindir}/%{name}
install -m 0755 src/util-lib.sh %{buildroot}/%{_bindir}/util-lib.sh
install -m 0755 src/date-lib.sh %{buildroot}/%{_bindir}/date-lib.sh
install -m 0755 src/mail-lib.sh %{buildroot}/%{_bindir}/mail-lib.sh
install -m 0755 src/setup-lib.sh %{buildroot}/%{_bindir}/setup-lib.sh

gzip man/%{name}.1 man/date-lib.3 man/mail-lib.3 man/setup-lib.3 man/util-lib.3

mkdir -p %{buildroot}/%{_mandir}/man1
install -m 0644 man/%{name}.1.gz %{buildroot}/%{_mandir}/man1/%{name}.1.gz

mkdir -p %{buildroot}/%{_mandir}/man3
install -m 0644 man/util-lib.3.gz %{buildroot}/%{_mandir}/man3/util-lib.3.gz
install -m 0644 man/date-lib.3.gz %{buildroot}/%{_mandir}/man3/date-lib.3.gz
install -m 0644 man/mail-lib.3.gz %{buildroot}/%{_mandir}/man3/mail-lib.3.gz
install -m 0644 man/setup-lib.3.gz %{buildroot}/%{_mandir}/man3/setup-lib.3.gz

%files
%{_bindir}/%{name}
%{_bindir}/util-lib.sh
%{_bindir}/date-lib.sh
%{_bindir}/mail-lib.sh
%{_bindir}/setup-lib.sh
%{_mandir}/man1/%{name}.1.gz
%{_mandir}/man3/util-lib.3.gz
%{_mandir}/man3/date-lib.3.gz
%{_mandir}/man3/mail-lib.3.gz
%{_mandir}/man3/setup-lib.3.gz

%changelog
* Tue Jan 28 2020 Logan Warner <logan@true.group> - 1.7-2
- Update automail-1 man file to reflect current documentation
- Remove redundent code
- Fix help message

* Tue Jan 28 2020 Logan Warner <logan@true.group> - 1.7-1
- Add ability to specify multiple files to be archived in each users mailbox (inbox, sent, etc)
- Add feature that prevents archives from growing bigger than 2GB by breaking it into multiple files.
- Minor code refactoring

* Wed Jan 22 2020 Logan Warner <logan@true.group> - 1.6-1
- Fix date Arithmatic
- Add --help and -? flags
- Fix -q to be silent, not just quiet, except on errors
- Fix automail to bail out if mailutil errors
- Add feature to prevent archiving the current month (or week in weekly schedule)

* Fri May 24 2019 Logan Warner <logan@true.group>
- First automail package
