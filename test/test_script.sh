#!/bin/bash

###############################################################
#source gen-lib.sh
# source ../automail/util-lib.sh
# echo "Enter how many days you want to test the script for:"
# read days

# echo "Enter the root mail directory"
# read dir

# for i in `seq 0 $days`
# do
#     #generate mail of size between 50KB and 5MB
#     echo a
#     faketime -f "+$(echo $i)d" ../test_framework/gen-lib.sh $dir 512 $((1024 * 5))
#     #echo z
#     #    faketime -f "+$i" ../automail/main.sh -a -l
#     faketime -f "+$(echo $i)d" ../automail/main.sh -a
#     echo @
# done
###############################################################



###############################################################
#GLOBAL VARIABLES
###############################################################
KB="1024"
MB="$(( $KB * $KB ))"
loc="/mail/test_framework"

###############################################################
#FUNCTIONS
###############################################################

function get_inbox_size
{
    echo $(du -sBM "$loc/$1/mail/inbox" | grep -o "^[0-9]*")
}

function send_inbox
{
    target_directory=$1
    size="$(($2 * $MB / 10))"
    date=( $(echo $3 | grep -o "[0-9][0-9]") )
    #echo "0: ${date[0]}/ 1:${date[1]}/ 2:${date[2]}/ 3:${date[3]}"
    for i in $(seq 0 10)
    do
	faketime "${date[0]}/${date[1]}/${date[2]}" ./gen-lib.sh "$target_directory" "$size"
	date[0]=$(( $(echo ${date[0]} | sed "s/^0//")  + 1 ))
	if [ ${date[0]} -lt 10 ]
	then
	    date[0]="0${date[0]}"
	fi
    done
}

function setup-inbox
{
    dir="$1/mail/inbox"
    echo "From MAILER-DAEMON Mon Jan 01  0:00:00 2001" > $dir
    echo "Date: 04 Jul 2018 11:30:30 +1200" >> $dir
    echo "From: Mail System Internal Data <MAILER-DAEMON@Nautilus>" >> $dir
    echo "Subject: DON'T DELETE THIS MESSAGE -- FOLDER INTERNAL DATA" >> $dir
    echo "Message-ID: <1530660630@Nautilus>" >> $dir
    echo "X-IMAP: 1530659967 0000000001" >> $dir
    echo "Status: RO" >> $dir
    echo "" >> $dir
    echo "This text is part of the internal format of your mail folder, and is not" >> $dir
    echo "a real message.  It is created automatically by the mail system software." >> $dir
    echo "If deleted, important folder data will be lost, and it will be re-created" >> $dir
    echo "with the data reset to initial values." >> $dir
    echo "" >> $dir
    echo "" >> $dir
}

function test_inbox
{
    #$loc replaces dir
    #dir=$1
    title="$1"
    inbox="$2"
    new_date="`date --date="$3" +%D`"
    echo $new_date
    setup-inbox "$loc/$inbox"

    #size of inbox before:
    size_before=$(get_inbox_size "$inbox")

    #gen_mail date: before last archive
    #faketime "07/03/17" ./gen_mail "$loc/sane/weekly/mail/inbox"
    send_inbox "$loc/$inbox/mail/inbox" 100 "01/01/18"

    size_during=$(get_inbox_size "$inbox")

    echo "starting main.sh"
    #automail date (American) 07/03/18 (Tuesday)
    faketime "$new_date" ./main.sh -a
    size_after=$(get_inbox_size "$inbox")


    echo "size before: $size_before"
    echo "size during: $size_during"
    echo "size after: $size_after"
    printf "$title archive test: "

}

###############################################################
#MAIN
###############################################################

  ##########################
  #SANE SCHEDULED TEST
  ##########################

test_inbox "Sane Scheduled Weekly" "/sane/weekly" "last tuesday"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Scheduled Monthly" "/sane/monthly" "1 jan"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Scheduled Quarterly" "/sane/quarterly" "4/1/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Scheduled Half Yearly" "/sane/half-yearly" "7/1/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Scheduled Yearly" "/sane/half-yearly" "12/31/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi


  ##########################
  #NOT-SANE SCHEDULED TEST
  ##########################

test_inbox "Not-Sane Scheduled Weekly" "/not-sane/weekly" "Last Tuesday"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Scheduled Monthly" "/not-sane/monthly" "1 jan"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Scheduled Quarterly" "/not-sane/quarterly" "4/1/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Scheduled Half Yearly" "/not-sane/half-yearly" "7/1/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Scheduled Yearly" "/not-sane/half-yearly" "12/31/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

  ##########################
  #SANE OVERFLOW TEST
  ##########################

test_inbox "Sane Overflow Weekly" "/sane/weekly" "last thursday"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Overlow Monthly" "/sane/monthly" "10 jan"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Overlow Quarterly" "/sane/quarterly" "3/5/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Overlow Half Yearly" "/sane/half-yearly" "5/9/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Sane Overlow Yearly" "/sane/half-yearly" "11/2/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi


  ##########################
  #NOT-SANE OVERLOW TEST
  ##########################

test_inbox "Not-Sane Overlow Weekly" "/not-sane/weekly" "Last Thursday"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Overlow Monthly" "/not-sane/monthly" "5 jan"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Overlow Quarterly" "/not-sane/quarterly" "3/5/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Overlow Half Yearly" "/not-sane/half-yearly" "5/3/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi

test_inbox "Not-Sane Overlow Yearly" "/not-sane/half-yearly" "11/9/18"

if [ $size_before -lt $size_during ] && [ $size_after -lt 100 ]
then
    echo -e "\t\tsuccess\n"
else
    echo -e "\t\tfailed\n"
fi
