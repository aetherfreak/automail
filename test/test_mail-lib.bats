#!/usr/bin/env bats

##############################################################
# mail-lib.sh test script
##############################################################

_bats_="1"
setup() {
    source ../src/setup-lib.sh
    source ../src/mail-lib.sh
    source ../src/util-lib.sh
    setup_global_vars

    #if domain1 not created create it
    if [ ! -f $(pwd)/mail-test-env/domain01/user01/mail/inbox ]
    then
	mailutil create $(pwd)/mail-test-env/domain01/user01/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain01/user02/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain01/user02/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain01/user03/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain01/user03/mail/inbox
    fi

    [ -f $(pwd)/mail-test-env/domain01/user01/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain01/user02/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain01/user03/mail/inbox ]

    #if domain 2 not created create it
    if [ ! -f $(pwd)/mail-test-env/domain02/user01/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain02/user01/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain02/user02/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain02/user02/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain02/user03/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain02/user03/mail/inbox
    fi

    [ -f $(pwd)/mail-test-env/domain02/user01/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain02/user02/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain02/user03/mail/inbox ]

    #If domain3 not created make it
    if [ ! -f $(pwd)/mail-test-env/domain03/user01/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain03/user01/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain03/user03/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain03/user02/mail/inbox
    fi
    if [ ! -f $(pwd)/mail-test-env/domain03/user03/mail/inbox ]
    then
    	mailutil create $(pwd)/mail-test-env/domain03/user03/mail/inbox
    fi

    [ -f $(pwd)/mail-test-env/domain03/user01/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain03/user02/mail/inbox ]
    [ -f $(pwd)/mail-test-env/domain03/user03/mail/inbox ]
}

reset_inbox() {
    rm $(pwd)/mail-test-env/domain01/user01/mail/inbox*
    mailutil create $(pwd)/mail-test-env/domain01/user01/mail/inbox
}

assert_mailbox_full() {
    #$1 path
    #$2 max
    local path="$(pwd)/$1"
    if [ "$(get_size "$path")" -lt "$2" ]
    then
	./faketime "$(date --date="-6 year")" ./gen-lib.sh "$path" 15728640
	./faketime "$(date --date="-3 months")" ./gen-lib.sh "$path" 15728640
	./faketime "$(date --date="-1 months")" ./gen-lib.sh "$path" 15728640
    fi
    [ "$(get_size "$path")" -gt "44" ]
}

@test "Remove all Logs from previous tests (not really a test)" {
    rm $(pwd)/logs/*
}

@test "Sanity check: bc 2+2=4" {

    result="$(echo 2+2 | bc)"
    [ "$result" -eq 4 ]
}

@test "Testing mailutil archive" {
    #skip
    
    _log_file_="logs/test_mail-lib_1.log"

    ./gen-lib.sh $(pwd)/mail-test-env/domain01/user01/mail/inbox 1000
    run mailutil archive "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "$(pwd)/mail-test-env/domain01/user01/mail/inbox2" "before 01/01/20"
    [ -f "$(pwd)/mail-test-env/domain01/user01/mail/inbox2" ]

    rm mail-test-env/domain01/user01/mail/inbox2
}

@test "test check_against_max with default values" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_2.log"

    local archive_enum=( "Y" "H" "Q" "M" "W" "abc" )
    local inbox_name_enum=(
	"$(inbox_YYYY)"
	"$(inbox_h)"
	"$(inbox_q)"
	"$(inbox_MM)"
	"$(inbox_WW)"
	"$(inbox_WW)"  # matches to the "abc", any invalid val should
	               #be treated as weekly... may change that to
	               #default to yearly later
    )

    #set vars
    _override_max_="0"
    _max_="14"
    _search_mod_="before"
    _search_date_="04/01/17"

    # for each archive_enum check that it archives the inbox and names
    # it to the corresponding name
    for i in $(seq 0 $(( ${#archive_enum[@]} - 1 )) )
    do
	assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45

	_archive_type_="${archive_enum[$i]}"
	run check_against_max "$(pwd)/mail-test-env/domain01/user01" "0"

	for i in $(pwd)/mail-test-env/domain01/user01/mail/${inbox_name_enum[$i]}*
	do
	    [ -f "$i" ]
	done
	    
	[ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    done
    reset_inbox
}


@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: < _size_, _override_max_: inactive, archive: true" {
    #skip
    
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_3.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=0
    _max_=5

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_1.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "$_max_" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: > _size_, _override_max_: inactive, archive: false" {
    #skip
    
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_4.log"
    
    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=0
    _max_=20

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo $output > "logs/CAM_MO_2.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -eq "16" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: inactive, _override_max_: inactive; archive: false" {
    #skip
    
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_5.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=0
    _max_=0

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_3.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -eq "16" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: < size, _override_max_: < size; archive: true" {
    #skip
    
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_6.log"
    
    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=5
    _max_=5

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_4.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "$_max_" ] # could equally test against _override_max_
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: > s, _override_max_: < s; archive: true" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_7.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=5
    _max_=20

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_5.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "$_override_max_" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: inactive, _override_max_: < s; archive: true" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_8.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=5
    _max_=0

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_6.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "$_override_max_" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: < s, _override_max_: > s; archive: false" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_9.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=20
    _max_=5

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_7.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -eq "16" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: > s, _override_max_: > s; archive: false" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_10.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=20
    _max_=20

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_8.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -eq "16" ]
    reset_inbox
}

@test "test check_against_max _override_max_ and _max_ archive truth table. _max_: inactive, _override_max_: > s; archive: false" {
    #skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_11.log"

    _archive_type_="Y"
    _search_mode_="before"
    _search_date_="04/01/17"

    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 14 ]
    then
	./faketime "01/01/17" ./gen-lib.sh "$(pwd)/mail-test-env/domain01/user01/mail/inbox" "15728640"
    fi

    _override_max_=20
    _max_=0

    run check_against_max "$(pwd)/mail-test-env/domain01/user01/" "0"
    echo "$output" > "logs/CAM_MO_9.log"
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -eq "16" ]
    reset_inbox
}

@test "Test that archive doesn't try to use mailutil archive if inbox.lock exists" {
    skip
    
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_12.log"
    
    touch "$(pwd)/mail-test-env/domain01/user01/mail/inbox.lock"
    run archive_inbox "$(pwd)/mail-test-env/domain01/user01/" "0" "before 01/01/01" "lock_test"
    rm "$(pwd)/mail-test-env/domain01/user01/mail/inbox.lock"
    [ ! -f "$(pwd)/mail-test-env/domain01/user01/mail/inbox.lock" ]
    [ ! -f "$(pwd)/mail-test-env/domain01/user01/mail/lock_test" ]
    [ -n "$(echo "$output" | grep "inbox is locked. Waiting 10 seconds...")" ]
    [ -n "$(echo "$output" | grep "Skipping inbox." )" ]
}


@test "test establish_archive_policy with . policy" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_13.log"
    _config_file_=(
	2000
	
	"." #domain
	"." #user
	"." #schedule
	"." #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    #echo $_archive_type_ > establish_archive_policy_logs/test1.log

    [ -n "$(grep -o "_domain_:" "logs/test_mail-lib_13.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_13.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_13.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_13.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_13.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_13.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}

@test "test establish_archive_policy with not matching domain, but matching user" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_14.log"
    _config_file_=(
	2000
	
	"random" #domain
	"user" #user
	"." #schedule
	"." #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    #echo $_archive_type_ > establish_archive_policy_logs/test1.log

    [ -z "$(grep -o "_domain_: domain" "logs/test_mail-lib_14.log")" ]
    [ -z "$(grep -o "_user_:" "logs/test_mail-lib_14.log")" ]
    [ -z "$(grep -o "_archive_type_:" "logs/test_mail-lib_14.log")" ]
    [ -z "$(grep -o "_max_:" "logs/test_mail-lib_14.log")" ]
    [ -z "$(grep -o "_search_mod_:" "logs/test_mail-lib_14.log")" ]
    [ -z "$(grep -o "_search_date_:" "logs/test_mail-lib_14.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}

@test "test establish_archive_policy with matching domain but different user" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_15.log"
    _config_file_=(
	2000
	
	"domain" #domain
	"not_make_it_past_this_point" #user
	"." #schedule
	"." #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    #echo $_archive_type_ > establish_archive_policy_logs/test3.log

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_15.log")" ]
    [ -z "$(grep -o "_user_:" "logs/test_mail-lib_15.log")" ]
    [ -z "$(grep -o "_archive_type_:" "logs/test_mail-lib_15.log")" ]
    [ -z "$(grep -o "_max_:" "logs/test_mail-lib_15.log")" ]
    [ -z "$(grep -o "_search_mod_:" "logs/test_mail-lib_15.log")" ]
    [ -z "$(grep -o "_search_date_:" "logs/test_mail-lib_15.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}


@test "test establish_archive_policy with matching user and domain and . attributes" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_16.log"
    _config_file_=(
	2000
	
	"domain" #domain
	"user" #user
	"." #schedule
	"." #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_16.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_16.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_16.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_16.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_16.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_16.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}

@test "test establish_archive_policy with matching user and domain and specific schedule attribute and remaining . attributes" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_17.log"
    _config_file_=(
	2000
	
	"domain" #domain
	"user" #user
	"H" #schedule
	"." #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_17.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_17.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_17.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_17.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_17.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_17.log")" ]
    [ "$_archive_type_" = "H" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}

@test "test establish_archive_policy with matching user and domain and specific max attribute and remaining . attributes" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_18.log"
    _config_file_=(
	15
	
	"domain" #domain
	"user" #user
	"." #schedule
	"100" #max
	"." #search
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_18.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_18.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_18.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_18.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_18.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_18.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "100" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
}

@test "test establish_archive_policy with matching user and domain and specific search_mod and search_date attributes and remaining . attributes" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_19.log"
    _config_file_=(
	2000
	
	"domain" #domain
	"user" #user
	"." #schedule
	"." #max
	"since" #search_mod
	"-1/-1/-1" #search_date
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_19.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_19.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_19.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_19.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_19.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_19.log")" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_max_" -eq "2000" ]
    [ "$_search_mod_" = "since" ]
    [ "$_search_date_" = "-1/-1/-1" ]
}

@test "test establish_archive_policy with matching user and domain and specific attributes" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_20.log"
    _config_file_=(
	2000
	
	"domain" #domain
	"user" #user
	"M" #schedule
	"780" #max
	"since" #search_mod
	"-1/-1/-1"
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_20.log")" ]
    [ -n "$(grep -o "_user_:" "logs/test_mail-lib_20.log")" ]
    [ -n "$(grep -o "_archive_type_:" "logs/test_mail-lib_20.log")" ]
    [ -n "$(grep -o "_max_:" "logs/test_mail-lib_20.log")" ]
    [ -n "$(grep -o "_search_mod_:" "logs/test_mail-lib_20.log")" ]
    [ -n "$(grep -o "_search_date_:" "logs/test_mail-lib_20.log")" ]
    [ "$_archive_type_" = "M" ]
    [ "$_max_" -eq "780" ]
    [ "$_search_mod_" = "since" ]
    [ "$_search_date_" = "-1/-1/-1" ]
}

@test "test establish_archive_policy with multiple domains, users, etc in config file" {
    #skip
    
    source ../src/setup-lib.sh
    setup_global_vars

    _log_file_="logs/test_mail-lib_21.log"
    _config_file_=(
	15
	# Override Defaults
	"." #domain
	"." #user
	"W" #schedule
	"90" #max
	"before" #search_mod
	"-1/-1/-1"

	"domain1"
	"user2"
	"M"
	"1000"
	"since"
	"2/2/2"

	"domain"
	"."
	"."
	"200"
	"."

	"domain"
	"user"
	"Q"
	"."
	"."

	"."
	"user"
	"M"
	"."
	"since"
	"-3/0/0"
    )	

    establish_archive_policy "domain" "user"

    [ -n "$(grep -o "_domain_: domain" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_domain_: ." "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_user_: user" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_user_: ." "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_archive_type_: Q" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_archive_type_: M" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_max_: 200" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_search_mod_: since" "logs/test_mail-lib_21.log")" ]
    [ -n "$(grep -o "_search_date_: -3/0/0" "logs/test_mail-lib_21.log")" ]
    [ "$_archive_type_" = "M" ]
    [ "$_max_" -eq "200" ]
    [ "$_search_mod_" = "since" ]
    [ "$_search_date_" = "-3/0/0" ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = Y with _archive_yearly_ = $(date +%_j) returns 1" {
    #skip
    
    _log_file_="logs/test_mail-lib_22.log"   
    _archive_type_="Y"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -n "$(grep -o "for yearly archiving" "logs/test_mail-lib_22.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_22.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_22.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_22.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_22.log")" ]
    [ $status -eq 1 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = H with _archive_half_yearly_ = $(date +%_j) returns 1" {
    #skip
    
    _log_file_="logs/test_mail-lib_23.log"
    _archive_type_="H"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_23.log")" ]
    [ -n "$(grep -o "half yearly archiving" "logs/test_mail-lib_23.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_23.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_23.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_23.log")" ]
    [ $status -eq 1 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = Q with _archive_quarterly_ = $(date +%_j) returns 1" {
    #skip
    
    _log_file_="logs/test_mail-lib_24.log"
    _archive_type_="Q"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_24.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_24.log")" ]
    [ -n "$(grep -o "quarterly archiving" "logs/test_mail-lib_24.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_24.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_24.log")" ]
    [ $status -eq 1 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = M with _archive_monthly_ = $(date +%_d) returns 1" {
    #skip
    
    _log_file_="logs/test_mail-lib_25.log"
    _archive_type_="M"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_25.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_25.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_25.log")" ]
    [ -n "$(grep -o "monthly archiving" "logs/test_mail-lib_25.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_25.log")" ]
    [ $status -eq 1 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = W with _archive_weekly_ = $(date +%_u) returns 1" {
    #skip
    
    _log_file_="logs/test_mail-lib_26.log"
    _archive_type_="W"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_26.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_26.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_26.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_26.log")" ]
    [ -n "$(grep -o "weekly archiving" "logs/test_mail-lib_26.log")" ]
    [ $status -eq 1 ]
}

############## tests where check_against_archive_policy not due ###############

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = Y with _archive_yearly_ = 0 returns 0" {
    #skip
    
    _log_file_="logs/test_mail-lib_27.log"
    _archive_type_="Y"
    _archive_yearly_="0"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_27.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_27.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_27.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_27.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_27.log")" ]
    [ $status -eq 0 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = H with _archive_half_yearly_ = 500 returns 0" {
    #skip
    
    _log_file_="logs/test_mail-lib_28.log"
    _archive_type_="H"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="500"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_28.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_28.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_28.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_28.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_28.log")" ]
    [ $status -eq 0 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = Q with _archive_quarterly_ = 500 returns 0" {
    #skip
    
    _log_file_="logs/test_mail-lib_29.log"
    _archive_type_="Q"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="500"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_29.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_29.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_29.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_29.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_29.log")" ]
    [ $status -eq 0 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = M with _archive_monthly_ = 0 returns 0" {
    #skip
    
    _log_file_="logs/test_mail-lib_30.log"
    _archive_type_="M"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="0"
    _archive_weekly_="$(date +%u)"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_30.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_30.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_30.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_30.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_30.log")" ]
    [ $status -eq 0 ]
}

@test "check_against_archive_policy with archive_repeat = 0 and _archive_type_ = W with _archive_weekly_ = 0 returns 0" {
    #skip
    _log_file_="logs/test_mail-lib_31.log"
    _archive_type_="W"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="0"

    run check_against_archive_policy "test_path"

    [ -z "$(grep -o "for yearly archiving" "logs/test_mail-lib_31.log")" ]
    [ -z "$(grep -o "half yearly archiving" "logs/test_mail-lib_31.log")" ]
    [ -z "$(grep -o "quarterly archiving" "logs/test_mail-lib_31.log")" ]
    [ -z "$(grep -o "monthly archiving" "logs/test_mail-lib_31.log")" ]
    [ -z "$(grep -o "weekly archiving" "logs/test_mail-lib_31.log")" ]
    [ $status -eq 0 ]
}


@test "iterate_over_users test, where a single inbox is specified and it is scheduled for archiving and greater than max, this should create archive" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_32.log"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 46
    
    _user_="user01"
    
    run iterate_over_users "$(pwd)/mail-test-env/domain01"
    #echo "$output" > "logs/iterate_over_users_1.log"

    [ -z "$(grep -o "user03" "logs/test_mail-lib_32.log")" ]
    [ -z "$(grep -o "user02" "logs/test_mail-lib_32.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_32.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_32.log")" ]
    [ -n "$(grep -o "for yearly" "logs/test_mail-lib_32.log")" ]
    #testing the "since" search criteria kinda makes things slightly messy.
    #First everything after the initial date is archived, then everything
    #after that date, requiring only two operations.
    #[ -n "$(grep -o "archive_repeat: 1" "logs/test_mail-lib_32.log")" ]
    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
}

@test "iterate_over_users check that archive created if scheduled but not oversize" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_33.log"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )
    [ -f "$(pwd)/mail-test-env/domain01/user01/mail/inbox" ]
    if [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -gt "14" ]
    then
	rm "$(pwd)/mail-test-env/domain01/user01/mail/inbox"
    fi


    [ "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "44" ]
    _user_="user01"
    
    run iterate_over_users "$(pwd)/mail-test-env/domain01"
    echo "$output" > "logs/iterate_over_users_2.log"

    [ -z "$(grep -o "user03" "logs/test_mail-lib_33.log")" ]
    [ -z "$(grep -o "user02" "logs/test_mail-lib_33.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_33.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_33.log")" ]
    [ -n "$(grep -o "for yearly" "logs/test_mail-lib_33.log")" ]
    [ -z "$(grep -o "archive_repeat: 2" "logs/test_mail-lib_33.log")" ]
    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
}


@test "iterate_over_users check that archive created if only oversize" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_34.log"
    _archive_yearly_="$(( $(date +%_j) - 10 ))" #make sure it is not scheduled
    _archive_half_yearly_="$(( $(date +%_j) - 10 ))"
    _archive_quarterly_="$(( $(date +%_j) - 10 ))"
    _archive_monthly_="$(( $(date +%_d) - 11 ))"
    _archive_weekly_="$(( $(date +%u) - 1 ))"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45


    [ ! "$(get_size "$(pwd)/mail-test-env/domain01/user01/mail/inbox")" -lt "44" ]
    _user_="user01"
    
    run iterate_over_users "$(pwd)/mail-test-env/domain01"
    #echo "$output" > "logs/iterate_over_users_3.log"

    [ -z "$(grep -o "user03" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "user02" "logs/test_mail-lib_34.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_34.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "for yearly" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "for monthly" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "for weekly" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "for half yearly" "logs/test_mail-lib_34.log")" ]
    [ -z "$(grep -o "quarterly" "logs/test_mail-lib_34.log")" ]
    [ -n "$(grep -o "archive_repeat: 1" "logs/test_mail-lib_34.log")" ]
    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
}


@test "iterate_over_users check that it iterates over users and archives them correctly. Each user not affected by each other when scheduled" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_35.log"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(date +%_d)"
    _archive_weekly_="$(date +%u)"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user03/mail/inbox" 45
    
    run iterate_over_users "$(pwd)/mail-test-env/domain01"

    [ -n "$(grep -o "user03" "logs/test_mail-lib_35.log")" ]
    [ -n "$(grep -o "user02" "logs/test_mail-lib_35.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_35.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_35.log")" ]

    [ -n "$(grep -o "for weekly" "logs/test_mail-lib_35.log")" ]
    [ -n "$(grep -o "for monthly" "logs/test_mail-lib_35.log")" ]
    [ -n "$(grep -o "for yearly" "logs/test_mail-lib_35.log")" ]

    #[ -n "$(grep -o "archive_repeat: 1" "logs/test_mail-lib_35.log")" ]

    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user02/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user03/mail/inbox")" -lt 15 ]
}

@test "iterate_over_users check that it iterates over users and archives them correctly. Each user not affected by each other when not scheduled" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_36.log"
    _archive_yearly_="$(( $(date +%_j) + 10 ))"
    _archive_half_yearly_="(( $(date +%_j) + 10 ))"
    _archive_quarterly_="(( $(date +%_j) + 10 ))"
    _archive_monthly_="(( $(date +%_d) + 10 ))"
    _archive_weekly_="(( $(date +%_j) + 10 ))"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user03/mail/inbox" 45

    run iterate_over_users "$(pwd)/mail-test-env/domain01"

    [ -n "$(grep -o "user03" "logs/test_mail-lib_36.log")" ]
    [ -n "$(grep -o "user02" "logs/test_mail-lib_36.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_36.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_36.log")" ]

    [ -z "$(grep -o "for weekly" "logs/test_mail-lib_36.log")" ]
    [ -z "$(grep -o "for monthly" "logs/test_mail-lib_36.log")" ]
    [ -z "$(grep -o "for yearly" "logs/test_mail-lib_36.log")" ]

    [ -n "$(grep -o "archive_repeat: 2" "logs/test_mail-lib_36.log")" ]

    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user02/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user03/mail/inbox")" -lt 15 ]
}


@test "iterate_over_users check that it iterates over users and archives them correctly. Each user not affected by each other when some scheduled and some not" {
    #skip
    source ../src/date-lib.sh
    _log_file_="logs/test_mail-lib_37.log"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(( $(date +%_d) + 10 ))"
    _archive_weekly_="$(date +%u)"
    _directory_="$(pwd)"

    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"14"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user03/mail/inbox" 45

    run iterate_over_users "$(pwd)/mail-test-env/domain01"

    [ -n "$(grep -o "user03" "logs/test_mail-lib_37.log")" ]
    [ -n "$(grep -o "user02" "logs/test_mail-lib_37.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_37.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_37.log")" ]

    [ -n "$(grep -o "for weekly" "logs/test_mail-lib_37.log")" ]
    [ -z "$(grep -o "for monthly" "logs/test_mail-lib_37.log")" ]
    [ -n "$(grep -o "for yearly" "logs/test_mail-lib_37.log")" ]

    [ -n "$(grep -o "archive_repeat: 1" "logs/test_mail-lib_37.log")" ]

    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user02/mail/inbox")" -lt 15 ]
    [ "$(get_size "mail-test-env/domain01/user03/mail/inbox")" -lt 15 ]
}

@test "Test that iterate_over_domains only applies iterate_over_users to a single specified domain" {
    ##skip
    source ../src/util-lib.sh
    source ../src/date-lib.sh
    source ../src/setup-lib.sh

    setup_global_vars

    _log_file_="logs/test_mail-lib_38.log"
    _directory_="$(pwd)/mail-test-env"
    _domain_="domain01"
    
    run iterate_over_domains
    [ -n "$(grep -o "$(pwd)/mail-test-env/domain01" "logs/test_mail-lib_38.log")" ]
    [ -z "$( grep -o "$(pwd)/mail-test-env/domain02" "logs/test_mail-lib_38.log")" ]
    [ -z "$(grep -o "$(pwd)/mail-test-env/domain03" "logs/test_mail-lib_38.log")" ]
}

#I'm not sure what logic I was following but these seems a weird thing to test and potentially opposite of the behaviour we want to implement. Also I'm fairly sure that this circumstance can never happen due to the way set_flags is implemented
# @test "Test that iterate_over_domains only applies iterate_over_users to a single domain even if multiple specified and seperated by spaces" {
#     skip
#     source ../src/util-lib.sh
#     source ../src/date-lib.sh
#     source ../src/setup-lib.sh

#     setup_global_vars

#     _log_file_="logs/test_mail-lib_39.log"
#     _directory_="$(pwd)"
#     _domain_="domain01 domain02"
    
#     run iterate_over_domains
#     [ -n "$(grep -o "domain01 domain02" "logs/test_mail-lib_39.log")" ]
#     [ -z "$(grep -o "domain03" "logs/test_mail-lib_39.log")" ]
# }

@test "Test that iterate_over_domains applies to iterate_over_users to all domains if unspecified" {
    ##skip
    source ../src/util-lib.sh
    source ../src/date-lib.sh
    source ../src/setup-lib.sh

    setup_global_vars

    _log_file_="logs/test_mail-lib_40.log"
    _directory_="$(pwd)/mail-test-env"
    _domain_=""
    
    run iterate_over_domains
    [ -n "$(grep -o "domain01" "logs/test_mail-lib_40.log")" ]
    [ -n "$(grep -o "domain02" "logs/test_mail-lib_40.log")" ]
    [ -n "$(grep -o "domain03" "logs/test_mail-lib_40.log")" ]

    _domain_="0"
    
    run iterate_over_domains
    [ -n "$(grep -o "domain01" "logs/test_mail-lib_40.log")" ]
    [ -n "$(grep -o "domain02" "logs/test_mail-lib_40.log")" ]
    [ -n "$(grep -o "domain03" "logs/test_mail-lib_40.log")" ]
}

@test "Proper full archive test from iterate_over_domains" {
    #skip
    source ../src/util-lib.sh
    source ../src/date-lib.sh
    source ../src/setup-lib.sh
    _log_file_="logs/test_mail-lib_41.log"
    _archive_yearly_="$(date +%_j)"
    _archive_half_yearly_="$(date +%_j)"
    _archive_quarterly_="$(date +%_j)"
    _archive_monthly_="$(( $(date +%_d)))"
    _archive_weekly_="$(date +%u)"

    _directory_="$(pwd)/mail-test-env"
    
    _config_file_=(
	15
	
	#Override Defaults
	"." #domain
	"." #user
	"Y" #schedule
	"100" #max
	"before" #search mod
	"-6/0/-1" #search date

	"domain01"
	"."
	"."
	"15"
	"."

	"domain01"
	"user01"
	"."
	"."
	"since"
	"-3/0/-1"

	"domain01"
	"user02"
	"M"
	"."
	"."

	"domain01"
	"user03"
	"W"
	"."
	"before"
	"-6/0/0"

	"domain02"
	"."
	"M"
	"."
	"."

	"domain03"
	"."
	"H"
	"1"
	"."
    )

    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user03/mail/inbox" 45

    assert_mailbox_full "mail-test-env/domain02/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user03/mail/inbox" 45

    assert_mailbox_full "mail-test-env/domain03/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain03/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain03/user03/mail/inbox" 45

    run iterate_over_domains
    #iterate_over_domains

    [ -n "$(grep -o "user03" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "user02" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "user01" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "domain03" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "domain02" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "domain01" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "mail-test-env/domain01" "logs/test_mail-lib_41.log")" ]

    [ -n "$(grep -o "for weekly" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "for monthly" "logs/test_mail-lib_41.log")" ]
    [ -n "$(grep -o "for yearly" "logs/test_mail-lib_41.log")" ]

    #[ -n "$(grep -o "archive_repeat: 1" "logs/test_mail-lib_41.log")" ]

    [ "$(get_size "mail-test-env/domain01/user01/mail/inbox")" -lt 16 ]
    [ "$(get_size "mail-test-env/domain01/user02/mail/inbox")" -lt 16 ]
    [ "$(get_size "mail-test-env/domain01/user03/mail/inbox")" -lt 16 ]

    [ "$(get_size "mail-test-env/domain02/user01/mail/inbox")" -lt 100 ]
    [ "$(get_size "mail-test-env/domain02/user02/mail/inbox")" -lt 100 ]
    [ "$(get_size "mail-test-env/domain02/user03/mail/inbox")" -lt 100 ]

    [ "$(get_size "mail-test-env/domain03/user01/mail/inbox")" -lt 2 ]
    [ "$(get_size "mail-test-env/domain03/user02/mail/inbox")" -lt 2 ]
    [ "$(get_size "mail-test-env/domain03/user03/mail/inbox")" -lt 2 ]
}


@test "Test archive_specified_inboxes works with a single specified inbox" {
    ##skip

    source ../src/date-lib.sh
    setup_global_vars
    set_flags -i "$(pwd)/mail-test-env/domain01/user01" -m 15
    _log_file_="logs/test_mail-lib_43.log"
    
    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45

    run archive_specified_inboxes
}

@test "Test archive_specified_inboxes works with a multiple specified inboxes" {
    #skip

    source ../src/date-lib.sh
    setup_global_vars
    set_flags -i "$(pwd)/mail-test-env/domain01/user01" "$(pwd)/mail-test-env/domain01/user02" "$(pwd)/mail-test-env/domain02/user01" -m 15
    _log_file_="logs/test_mail-lib_44.log"
    
    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user01/mail/inbox" 45
    
    run archive_specified_inboxes
    
    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user02/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain02/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
}

@test "Test archive_specified_inboxes works with a multiple specified inboxes with config" {
    #skip

    source ../src/date-lib.sh
    setup_global_vars
    set_flags -i "$(pwd)/mail-test-env/domain01/user01" "$(pwd)/mail-test-env/domain01/user02" "$(pwd)/mail-test-env/domain02/user01" -c ./.config -a -s before $(date +%m/%d/%y)
    _log_file_="logs/test_mail-lib_45.log"
    
    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user01/mail/inbox" 45
    
    run archive_specified_inboxes

        [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user02/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain02/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
}

@test "Test archive_specified_inboxes works with a multiple specified inboxes with specified search dates" {
    #skip
    
    source ../src/date-lib.sh
    setup_global_vars
    set_flags -i "$(pwd)/mail-test-env/domain01/user01" "$(pwd)/mail-test-env/domain01/user02" "$(pwd)/mail-test-env/domain02/user01" -s before 0/-1/0 -m 15
    _log_file_="logs/test_mail-lib_46.log"
    
    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user01/mail/inbox" 45
    
    run archive_specified_inboxes

    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user02/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain02/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]

    [ -z "$(grep -o "archive_repeat: 2" logs/test_mail-lib_46.log)" ]
}

@test "Test archive_specified_inboxes works with a multiple specified inboxes with specified search dates and relative paths" {
    #skip
    
    source ../src/date-lib.sh
    setup_global_vars
    set_flags -i "mail-test-env/domain01/user01" "mail-test-env/domain01/user02" "mail-test-env/domain02/user01" -s before 0/-1/0 -m 15
    _log_file_="logs/test_mail-lib_47.log"
    
    assert_mailbox_full "mail-test-env/domain01/user01/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain01/user02/mail/inbox" 45
    assert_mailbox_full "mail-test-env/domain02/user01/mail/inbox" 45
    
    run archive_specified_inboxes

    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain01/user02/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]
    [ "$(du -sBM "$(pwd)/mail-test-env/domain02/user01/mail/inbox" | grep -o "^[0-9]*")" -lt 16 ]

    [ -z "$(grep -o "archive_repeat: 2" logs/test_mail-lib_47.log)" ]
}
