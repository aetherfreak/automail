#!/usr/bin/env bats

##############################################################
# date-lib.sh test script
##############################################################

_bats_="1"

setup() {
    source ../src/date-lib.sh
}

date() {
    ./faketime $_date_ /bin/date $@
}


@test "Sanity check: bc 2+2=4" {
    result="$(echo 2+2 | bc)"
    [ "$result" -eq 4 ]
}

@test "inbox_YYYY should return a name like 'inbox-2018-Y'" {
    
    _date_="01/01/01"
    [ "$(inbox_YYYY)" = "inbox-2001-Y" ]

    _date_="12/31/01"
    [ "$(inbox_YYYY)" = "inbox-2001-Y" ]

    _date_="06/21/1980"
    [ "$(inbox_YYYY)" = "inbox-1980-Y" ]

    _date_="04/01/34"
    [ "$(inbox_YYYY)" = "inbox-2034-Y" ]
}

@test "inbox_h should return a name like inbox-2018-H1 or inbox-2018-H2" {
    
    source ../src/util-lib.sh
    
    _date_="01/01/01"
    [ "$(inbox_h)" = "inbox-2001-H1" ]

    _date_="12/31/01"
    [ "$(inbox_h)" = "inbox-2001-H2" ]

    _date_="06/21/1980"
    [ "$(inbox_h)" = "inbox-1980-H1" ]

    _date_="07/01/34"
    [ "$(inbox_h)" = "inbox-2034-H2" ]

    #check for octal error
    _date_="08/01/34"
    [ "$(inbox_h)" = "inbox-2034-H2" ]

    _date_="09/01/34"
    [ "$(inbox_h)" = "inbox-2034-H2" ]
}

@test "inbox_q should return a name like inbox-2018-Q1 or inbox-2018-Q4..." {
    
    _date_="12/31/00"
    [ "$(inbox_q)" = "inbox-2000-Q4" ]

    _date_="01/01/01"
    [ "$(inbox_q)" = "inbox-2001-Q1" ]

    _date_="03/31/01"
    [ "$(inbox_q)" = "inbox-2001-Q1" ]

    _date_="04/01/01"
    [ "$(inbox_q)" = "inbox-2001-Q2" ]

    _date_="06/30/01"
    [ "$(inbox_q)" = "inbox-2001-Q2" ]

    _date_="07/01/01"
    [ "$(inbox_q)" = "inbox-2001-Q3" ]

    _date_="09/30/01"
    [ "$(inbox_q)" = "inbox-2001-Q3" ]

    _date_="10/01/01"
    [ "$(inbox_q)" = "inbox-2001-Q4" ]
}

@test "inbox_MM should return a name like inbox-2018-M07" {
    
    _date_="04/01/01"
    [ "$(inbox_MM)" = "inbox-2001-M04" ]

    _date_="04/30/01"
    [ "$(inbox_MM)" = "inbox-2001-M04" ]

    _date_="12/01/1989"
    [ "$(inbox_MM)" = "inbox-1989-M12" ]

    _date_="12/31/1989"
    [ "$(inbox_MM)" = "inbox-1989-M12" ]

    _date_="01/01/23"
    [ "$(inbox_MM)" = "inbox-2023-M01" ]

    _date_="01/31/23"
    [ "$(inbox_MM)" = "inbox-2023-M01" ]
}

@test "inbox_WW should return a name like inbox-2018-W01 or inbox-2018-W52" {
    
    _date_="05/07/18"
    [ "$(inbox_WW)" = "inbox-2018-W19" ]

    _date_="05/06/18"
    [ "$(inbox_WW)" = "inbox-2018-W18" ]

    _date_="05/08/18"
    [ "$(inbox_WW)" = "inbox-2018-W19" ]

    _date_="12/31/68"
    [ "$(inbox_WW)" = "inbox-2068-W53" ]

    _date_="01/01/69"
    [ "$(inbox_WW)" = "inbox-1969-W00" ]
}

@test "gen_archive_name with _archive_type_ of Y produces sane names" {
    
    _archive_type_="Y"

    _date_="07/25/18"
    [ $(gen_archive_name 0) = "inbox-2018-Y" ]
    [ $(gen_archive_name 1) = "inbox-2018-Y_archived-07-25-18" ]
    [ $(gen_archive_name 87) = "inbox-2018-Y_archived-07-25-18" ]

    _date_="12/31/69"
    [ $(gen_archive_name 0) = "inbox-1969-Y" ]
    [ $(gen_archive_name 10) = "inbox-1969-Y_archived-12-31-69" ]
    [ $(gen_archive_name 50) = "inbox-1969-Y_archived-12-31-69" ]
}

@test "gen_archive_name with _archive_type_ of H produces sane names" {
    
    source ../src/util-lib.sh
    _archive_type_="H"

    _date_="01/01/68"
    [ $(gen_archive_name 0) = "inbox-2068-H1" ]
    [ $(gen_archive_name 23) = "inbox-2068-H1_archived-01-01-68" ]
    [ $(gen_archive_name 48) = "inbox-2068-H1_archived-01-01-68" ]

    _date_="07/05/34"
    [ $(gen_archive_name 0) = "inbox-2034-H2" ]
    [ $(gen_archive_name 13) = "inbox-2034-H2_archived-07-05-34" ]
    [ $(gen_archive_name 10048) = "inbox-2034-H2_archived-07-05-34" ]
}

@test "gen_archive_name with _archive_type_ of Q produces sane names" {
    
    _archive_type_="Q"

    _date_="12/31/54"
    [ $(gen_archive_name 0) = "inbox-2054-Q4" ]
    [ $(gen_archive_name 14) = "inbox-2054-Q4_archived-12-31-54" ]
    [ $(gen_archive_name 37) = "inbox-2054-Q4_archived-12-31-54" ]

    _date_="08/05/34"
    [ $(gen_archive_name 0) = "inbox-2034-Q3" ]
    [ $(gen_archive_name 13) = "inbox-2034-Q3_archived-08-05-34" ]
    [ $(gen_archive_name 10048) = "inbox-2034-Q3_archived-08-05-34" ]
}

@test "gen_archive_name with _archive_type_ of W produces sane names" {
    
    _archive_type_="W"

    _date_="10/03/23"
    [ $(gen_archive_name 0) = "inbox-2023-W40" ]
    [ $(gen_archive_name 19) = "inbox-2023-W40_archived-10-03-23" ]
    [ $(gen_archive_name 42) = "inbox-2023-W40_archived-10-03-23" ]

    _date_="03/15/07"
    [ $(gen_archive_name 0) = "inbox-2007-W11" ]
    [ $(gen_archive_name 46) = "inbox-2007-W11_archived-03-15-07" ]
    [ $(gen_archive_name 102) = "inbox-2007-W11_archived-03-15-07" ]
}

@test "Testing that set_date_modifier generates the correct modifier" {
    _log_file_="logs/test_date-lib_1.log"
    _date_="07/25/18"

    _search_mod_="since"
    _search_date_="-1/-1/-1"
    [ "$(set_date_modifier 0)" = "since -1 -1 -1" ]

    _search_date_="-12/-34/-9"
    [ "$(set_date_modifier 0)" = "since -12 -34 -9" ]

    _search_mod_="before"
    [ "$(set_date_modifier 0)" = "before -12 -34 -9" ]
    
    _search_date_="-1/-1/-1"
    [ "$(set_date_modifier 0)" = "before -1 -1 -1" ]

    #When archive_repeat specified always "before"
    _archive_type_="Y"
    [ "$(set_date_modifier 1)" = "before -11 -0 -0" ]
    [ "$(set_date_modifier 6)" = "before -6 -0 -0" ]
    [ "$(set_date_modifier 12)" = "before 0 -0 -0" ]
    
    _archive_type_="H"
    [ "$(set_date_modifier 1)" = "before -5 -0 -0" ]
    [ "$(set_date_modifier 3)" = "before -3 -0 -0" ]
    [ "$(set_date_modifier 6)" = "before 0 -0 -0" ]

    _archive_type_="Q"
    [ "$(set_date_modifier 1)" = "before -3 -0 -0" ]
    [ "$(set_date_modifier 2)" = "before -2 -0 -0" ]
    [ "$(set_date_modifier 3)" = "before -1 -0 -0" ]
    [ "$(set_date_modifier 4)" = "before 0 -0 -0" ]

    _archive_type_="M"
    [ "$(set_date_modifier 1)" = "before -1 7 -0" ]
    [ "$(set_date_modifier 2)" = "before -1 14 -0" ]
    [ "$(set_date_modifier 3)" = "before -1 21 -0" ]
    [ "$(set_date_modifier 4)" = "before -1 28 -0" ]
    [ "$(set_date_modifier 5)" = "before -1 35 -0" ]

    _archive_type_="W"
    [ "$(set_date_modifier 1)" = "before -0 -6 -0" ]
    [ "$(set_date_modifier 4)" = "before -0 -3 -0" ]
    [ "$(set_date_modifier 7)" = "before -0 0 -0" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'Y' relative date" {
    source ../src/util-lib.sh
    unset date
    _quiet_=0
    _verbose_=0
    _log_file_="logs/test_date-lib_2.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="Y"
    _search_mod_="since"

    _search_date_="-1/-34/-21"
    target_date="$(date --date="$(date) -1 month -34 day -21 year" +%D)"

    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -11 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 12)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 13)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'H'relative date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _log_file_="logs/test_date-lib_3.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="H"
    _search_mod_="since"

    _search_date_="-1/-34/-21"
    target_date="$(date --date="$(date) -1 month -34 day -21 year" +%D)"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -5 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 6)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 7)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'Q'relative date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _log_file_="logs/test_date-lib_4.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="Q"
    _search_mod_="since"

    _search_date_="-1/-34/-21"
    target_date="$(date --date="$(date) -1 month -34 day -21 year" +%D)"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -3 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 4)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 5)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'M' relative date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _log_file_="logs/test_date-lib_5.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="M"
    _search_mod_="since"

    _search_date_="-1/-34/-21"
    target_date="$(date --date="$(date) -1 month -34 day -21 year" +%D)"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -1 month 7 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) -1 month 28 day -0 year" +%D)"
    [ "$(replace_date 4)" = "before $target_date" ]

    target_date="$(date --date="$(date) -1 month 35 day -0 year" +%D)"
    [ "$(replace_date 5)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'W' relative date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _log_file_="logs/test_date-lib_6.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="W"
    _search_mod_="since"

    _search_date_="-1/-34/-21"
    target_date="$(date --date="$(date) -1 month -34 day -21 year" +%D)"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -0 month -6 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) -0 month 0 day -0 year" +%D)"
    [ "$(replace_date 7)" = "before $target_date" ]

    target_date="$(date --date="$(date) -0 month 1 day -0 year" +%D)"
    [ "$(replace_date 8)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'Y' absolute date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _absolute_date_="1"
    _log_file_="logs/test_date-lib_7.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="Y"
    _search_mod_="since"

    _search_date_="1/12/21"
    target_date="1/12/21"

    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -11 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 12)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 13)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'H'absolute date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _absolute_date_="1"
    _log_file_="logs/test_date-lib_8.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="H"
    _search_mod_="since"

    _search_date_="1/12/21"
    target_date="1/12/21"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -5 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 6)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 7)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'Q'absolute date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _absolute_date_="1"
    _log_file_="logs/test_date-lib_9.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="Q"
    _search_mod_="since"

    _search_date_="1/12/21"
    target_date="1/12/21"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -3 month -0 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) 0 month -0 day -0 year" +%D)"
    [ "$(replace_date 4)" = "before $target_date" ]

    target_date="$(date --date="$(date) +1 month -0 day -0 year" +%D)"
    [ "$(replace_date 5)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'M' absolute date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _absolute_date_="1"
    _log_file_="logs/test_date-lib_10.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="M"
    _search_mod_="since"

    _search_date_="1/12/21"
    target_date="1/12/21"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -1 month 7 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) -1 month 28 day -0 year" +%D)"
    [ "$(replace_date 4)" = "before $target_date" ]

    target_date="$(date --date="$(date) -1 month 35 day -0 year" +%D)"
    [ "$(replace_date 5)" = "before $target_date" ]
}

@test "Testing that replace date produces the correct date for _archive_type_ 'W' absolute date" {
    source ../src/util-lib.sh
    unset date
    _verbose_=0
    _quiet_=0
    _absolute_date_="1"
    _log_file_="logs/test_date-lib_11.log"
    [ "$(date --date="01/01/01" +%m/%d/%y)" = "01/01/01" ]

    _archive_type_="W"
    _search_mod_="since"

    _search_date_="1/12/21"
    target_date="1/12/21"
    [ "$(replace_date 0)" = "since $target_date" ]

    target_date="$(date --date="$(date) -0 month -6 day -0 year" +%D)"
    [ "$(replace_date 1)" = "before $target_date" ]

    target_date="$(date --date="$(date) -0 month 0 day -0 year" +%D)"
    [ "$(replace_date 7)" = "before $target_date" ]

    target_date="$(date --date="$(date) -0 month 1 day -0 year" +%D)"
    [ "$(replace_date 8)" = "before $target_date" ]
}
