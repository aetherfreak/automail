#!/usr/bin/env bats

##############################################################
# setup-lib.sh test script
##############################################################

_bats_="1"

setup() {
    source ../src/setup-lib.sh
    source ../src/util-lib.sh
    setup_global_vars
    log ""
}

@test "Sanity check: bc 2+2=4" {
    result="$(echo 2+2 | bc)"
    [ "$result" -eq 4 ]
}

@test "Test that setup_global_vars works properly" {
    [ "$_stack_trace_" = "main" ]
    [ "$_log_file_" = "logs/log-$(date +%Y-%m-%d).out" ]
    
    [ "$_archive_weekly_" -eq "2" ]
    [ "$_archive_monthly_" -eq "1" ]      #1ST DAY OF MONTH
    [ "$_archive_quarterly_" -eq "$(( 364 / 4 ))" ]   #day / 4. If % 0 then archive
    [ "$_archive_half_yearly_" -eq "$(( 364 / 2 ))" ] #days / 2. if % 0 then archive
    [ "$_archive_yearly_" -eq "365" ]   #last day of year

    [ "$_config_file_" -eq "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" -eq "0" ]
    [ "$_max_" -eq "2000" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" -eq "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "Test validate_config_file" {
    _log_file_="logs/test_setup-lib_2.log"

    _config_file_=( 0 )
    run validate_config_file
    [ -n "$(echo $output | grep -o 'No config file')" ]

    _config_file_=( 1 )
    echo -e "y\n" | validate_config_file > "$_log_file_"
    [ -n "$(grep -o "either does not exist, or " $_log_file_)" ]

    _config_file_=( 1 2 )
    validate_config_file > "$_log_file_"
    [ -n "$(grep -o 'config_file loaded' $_log_file_)" ]
    [ -n "$(grep -o 'directory: 1' $_log_file_)" ]
    
    _directory_="something"
    _config_file_=( 1 2 )
    echo -e "YES\n" | validate_config_file > "$_log_file_"
    [ -n "$(grep -o 'config_file loaded' $_log_file_)" ]
    [ -n "$(grep -o 'directory: 1' $_log_file_)" ]

    _directory_="something"
    _config_file_=( 1 2 )
    echo -e "no\n" | validate_config_file > "$_log_file_"
    [ -n "$(cat "$_log_file_" | grep -o 'config_file loaded')" ]
    [ -n "$(grep -o 'directory: something' $_log_file_)" ]
    
    _directory_="something"
    _config_file_=( 1 2 )
    echo -e "exit\n" | validate_config_file > "$_log_file_"
    [ -z "$(cat "$_log_file_" | grep -o 'config_file loaded')" ]
    [ -z "$(grep -o 'directory: 1' $_log_file_)" ]

    _config_file_=( 1 )
    echo -e "\n" | validate_config_file > "$_log_file_"
    [ -n "$(cat "$_log_file_" | grep -o "Ignoring config.")" ]
}

@test "Test that validate_flags works as it should with _user_ and _domain_" {
    _user_=1
    _domain_=0
    run validate_flags
    [ -n "$(echo "$output" | grep -o "cannot specify a user without a ")" ]

    _user_=1
    _domain_=1
    run validate_flags
    [ -z "$(echo $output | grep -o "cannot specify a user without a ")" ]

    _user_=0
    _domain_=1
    run validate_flags
    [ -z "$(echo $output | grep -o "cannot specify a user without a ")" ]
    
    _user_=0
    _domain_=0
    run validate_flags
    [ -z "$(echo $output | grep -o "cannot specify a user without a ")" ]
}

@test "Test that validate_flags works as it should with _absolute_path_ and _search_mod_ and _search_date_" {
    _absolute_date_=1
    _search_mod_="before"
    _search_date_="-6/0/0"
    run validate_flags
    [ -n "$(echo "$output" | grep -o "absolute date without an absolute")" ]

    _absolute_date_=1
    _search_mod_="before"
    _search_date_="-5/0/0"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "absolute date without an absolute")" ]

    _absolute_date_=1
    _search_mod_="since"
    _search_date_="-6/0/0"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "date")" ]

    _absolute_date_=0
    _search_mod_="before"
    _search_date_="-6/0/0"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "absolute date without an absolute")" ]

    _absolute_date_=0
    _search_mod_="before"
    _search_date_="-6/1/0"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "absolute date without an absolute")" ]
    
    _absolute_date_=0
    _search_mod_="since"
    _search_date_="-6/0/0"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "absolute date without an absolute")" ]

    _absolute_date_=0
    _search_mod_="since"
    _search_date_="-5/1/8"
    run validate_flags
    [ -z "$(echo "$output" | grep -o "absolute date without an absolute")" ]
}

@test "Test that validate_flags works as it should with _quiet_ and _verbose_" {
    _log_file_="logs/validate_flags_test.log"
    _quiet_=1
    _verbose_=0
    run validate_flags
    [ -z "$(echo "$output" | grep -o "quiet with verbose")" ]

    _quiet_=1
    _verbose_=1
    run validate_flags
    [ -n "$(tail -n 5 logs/validate_flags_test.log | grep -o "quiet with verbose")" ]

    _quiet_=0
    _verbose_=1
    run validate_flags
    [ -z "$(echo $output | grep -o "quiet with verbose")" ]
    
    _quiet_=0
    _verbose_=0
    run validate_flags
    [ -z "$(echo $output | grep -o "quiet with verbose")" ]
}

@test "test load_config_file" {
    # test fails if load_config_file called through run as run cleans
    # up variables after it exits.
    load_config_file ./.config
    # ${_config_file_[@]} wrapped up in subshell so that it can be
    # expanded then compared as a string.  without this it expands in
    # the if statement and kicks up a "too many arguments" error
    [ "$(echo ${_config_file_[@]})" = "/a/directory 15 example-domain-01 . Y . before -0/-0/-1 example-domain-01 user-01 W . before -6/-0/-0 example-domain-01 user-02 M 30 . example-domain-02 user-01 Q 45 since -0/-0/-1" ]
    
}

@test "test set_flags with config file" {
    set_flags -c ./.config

    [ "$_absolute_date_" -eq "0" ]
    # ${_config_file_[@]} wrapped up in subshell so that it can be
    # expanded then compared as a string.  without this it expands in
    # the if statement and kicks up a "too many arguments" error First
    # two elements removed as validate config stores them in variables
    # and removes them
    [ "$(echo ${_config_file_[@]})" = "15 example-domain-01 . Y . before -0/-0/-1 example-domain-01 user-01 W . before -6/-0/-0 example-domain-01 user-02 M 30 . example-domain-02 user-01 Q 45 since -0/-0/-1" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]	
    [ "$_directory_" = "/a/directory" ]
    [ "$_domain_" = "0" ]
    [ "$_max_" -eq "15" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with specified inboxes" {
    set_flags -i "domain01/user01 domain02/user02" 

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "domain01/user01 domain02/user02" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_max_" -eq "2000" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with directory" {
    set_flags -D "$(pwd)/mail-test-env"

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "$(pwd)/mail-test-env" ]
    [ "$_domain_" = "0" ]
    [ "$_max_" -eq "2000" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with domain" {
    set_flags -d "magical-domain"

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "magical-domain" ]
    [ "$_max_" -eq "2000" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with override max" {
    set_flags -m 1450

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_override_max_" -eq "1450" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with archive type" {
    set_flags -t "M"

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "M" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with search mod and date" {
    set_flags -s "since 18/35/68"

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "since" ]
    [ "$_search_date_" = "18/35/68" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with quiet" {
    set_flags -q

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "1" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with user" {
    run set_flags -u "user01"

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "0" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "0" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test set_flags with user and domain" {
    set_flags -d "domain01" -u "user01"
    

    [ "$_absolute_date_" -eq "0" ]
    [ "$(echo ${_config_file_[@]})" = "0" ]
    [ "$(echo ${_specified_inboxes_[@]})" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" = "domain01" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]	
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" = "user01" ]
    [ "$_verbose_" -eq "0" ]
}

@test "test _verbose_ in set_flags" {
    set_flags -v

    [ "$_absolute_date_" -eq "0" ]
    [ "$_config_file_" = "0" ]
    [ "$_directory_" = "/mail" ]
    [ "$_domain_" -eq "0" ]
    [ "$_max_" -eq "2000" ]
    [ "$_override_max_" -eq "0" ]
    [ "$_archive_type_" = "Y" ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-6/0/0" ]
    [ "$_quiet_" -eq "0" ]
    [ "$_user_" -eq "0" ]
    [ "$_verbose_" -eq "1" ]
}

@test "test set_flags except _verbose_ and absolute date" {
    skip
    _log_file_="logs/test_setup-lib_19.log"
    run set_flags -c ./.config -D "$(pwd)/mail-test-env" -d "domain01" -i "domain02/user01 domain03/user01" -M 200 -s before -01/-01/-01 -S -t "M" -u "user03"

    [ "$_absolute_date_" -eq "0" ]
    #[ "$(echo ${_config_file_[@]})" = "example-domain-01 . Y . before -0/-0/-1 example-domain-01 user-01 W . before -6/-0/-0 example-domain-01 user-02 M 1000 . example-domain-02 user-01 Q 1500 since -0/-0/-1" ]
    
    [ "$(grep -o "example-domain-01 . Y . before -0/-0/-1 example-domain-01 user-01 W . before -6/-0/-0 example-domain-01 user-02 M 1000 . example-domain-02 user-01 Q 1500 since -0/-0/-1" $_log_file_)" ]
    echo $_directory_ > f.txt
    [ "$_directory_" = "$(pwd)/mail-test-env" ]
    [ "$_domain_" = "domain01" ]
    [ "$_specified_inboxes_" = "domain02/user01 domain03/user01" ]
    [ "$_override_max_" -eq 200 ]
    [ "$_search_mod_" = "before" ]
    [ "$_search_date_" = "-01/-01/-01" ]
    [ "$_quiet_" -eq "1" ]
    [ "$_archive_type_" = "M" ]
    [ "$_user_" = "user03" ]
    
}
