#!/bin/bash
#Generate fake mail

#function send
#{
#    echo -e "$@" >> ~/inbox;
#}

KB="1024"
MB=$(( $KB * $KB ))

function gen_mail
{
    #echo generating mail
    inbox="$1"
    size_in_bytes="$2"
    
    echo -e "\r\n" >> $inbox
    echo "From AUTOMAIL-TEST-FRAMEWORK `date +"%a %b %d %k:%M:%S %Y"`" >> $inbox
    echo "Date: `date +"%a %d %b %Y %k:%M:%S %z"`" >> $inbox
    echo "From: Automail Test Data <testdata@automail>" >> $inbox
    echo "Subject: Here's some test data of $size size" >> $inbox

    #send "\'$(head -c $size_in_bytes /dev/urandom)\'"
    echo "$(perl -E "print '*' x $size_in_bytes")" >> $inbox
}

# function send_mail
# {
#     target_directory=$1
#     lower_bound="$(($2 * $KB))"
#     upper_bound="$(($3 * $KB))"
    
#     domain_list="$(ls $target_directory)"
#     for domain in $domain_list
#     do
# 	user_list="$(ls "$target_directory/$domain")"
# 	for user in $user_list
# 	do
# 	    size="$(($RANDOM % ($upper_bound - $lower_bound) + $lower_bound))"
# 	    gen_mail "$target_directory/$domain/$user/mail/inbox" "$size"
# 	done
#     done
# }
#gen_mail "/mail/domain1/user10/mail/inbox" 100 1024

gen_mail $1 $2
