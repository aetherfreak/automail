#!/usr/bin/env bats

##############################################################
# util-lib.sh test script
##############################################################

_bats_="1"

setup()
{
    source ../src/util-lib.sh
}

@test "Sanity check: bc 2+2=4" {
    result="$(echo 2+2 | bc)"
    [ "$result" -eq 4 ]
}

@test "test that the log function displays and stores properly into the log file" {
    _log_file_="logs/util-lib-test.log"
    _verbose_="0"
    
    run log "hello"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "hello")" = "hello" ]

    _verbose_="1"
    run log "goodbye"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "goodbye")" = "goodbye" ]
    [ "$(echo $output | grep -o "goodbye")" = "goodbye" ]
}

@test "test that log function works correctly with incr_stack and decr_stack" {
    _log_file_="logs/util-lib-test.log"
    _stack_trace_="main"

    run log "basic stack trace"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "\[main\]")" = "[main]" ]

    incr_stack "incr_stack"
    run log "incremented the stack trace"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "\[main/incr_stack\]")" = "[main/incr_stack]" ]

    decr_stack
    run log "decremented the stack"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "\[main\]")" = "[main]" ]

    decr_stack
    run log "decremented the stack again. Should be empty"
    [ "$(tail -n 1 logs/util-lib-test.log | grep -o "\[\]")" = "[]" ]
}

@test "test that the show function works" {
    _log_file_="logs/util-lib-test.log"
    _quiet_="0"
    
    run show "konbanwa"
    [ "$output" = "konbanwa" ]
    [ "$(tail -n 1 logs/util-lib-test.log)" != "konbanwa" ]

    _quiet_="1"
    run show "sayonara"
    [ "$output" != "sayonara" ]
    [ "$(tail -n 1 logs/util-lib-test.log)" = "sayonara" ]
}

@test "get_size should return 13 (rounds up)" {
    run get_size "test-file1"

    [ "$output" = "13" ]
}

@test "get_owner should return a value that matches ^.*:.*$" {
    run get_owner "test-file1"
    [[ "$output" =~ ^.*:.*$ ]]
}

@test "get_owner should return $USER:$USER for file" {
    run get_owner "test-file1"

    [ "$output" = "$USER:$USER" ]
}

@test "get_owner should return $USER:nobody for test-file2" {
    [ "$(stat -c %G test-file2)" = "nobody" ] # If fails please check whether test-file2 has owner $USER:nobody and fix it

    run get_owner "test-file2"

    [ "$output" = "$USER:nobody" ]
}

@test "incr should return n + 1" {
    [ $(incr 1) -eq 2 ]
    [ $(incr -1) -eq 0 ]
    [ $(incr 1024) -eq 1025 ]
    [ $(incr -1024) -eq -1023 ]
}

@test "num_multiple_range returns 1 if 0-6 and 2 if 7-12" {
    [ "$(num_multiple_range 1 6)" = "1" ]
    [ "$(num_multiple_range 2 6)" = "1" ]
    [ "$(num_multiple_range 3 6)" = "1" ]
    [ "$(num_multiple_range 4 6)" = "1" ]
    [ "$(num_multiple_range 5 6)" = "1" ]
    [ "$(num_multiple_range 6 6)" = "1" ]

    [ "$(num_multiple_range 7 6)" = "2" ]
    [ "$(num_multiple_range 8 6)" = "2" ]
    [ "$(num_multiple_range 9 6)" = "2" ]
    [ "$(num_multiple_range 10 6)" = "2" ]
    [ "$(num_multiple_range 11 6)" = "2" ]
    [ "$(num_multiple_range 12 6)" = "2" ]
}

@test "num_multiple_range returns the division rounded up" {
    [ "$(num_multiple_range 6 7)" = "1" ]
    [ "$(num_multiple_range 7 7)" = "1" ]
    [ "$(num_multiple_range 8 7)" = "2" ]

    [ "$(num_multiple_range 2 3)" = "1" ]
    [ "$(num_multiple_range 3 3)" = "1" ]
    [ "$(num_multiple_range 4 3)" = "2" ]
    [ "$(num_multiple_range 5 3)" = "2" ]
    [ "$(num_multiple_range 6 3)" = "2" ]
    [ "$(num_multiple_range 7 3)" = "3" ]
    [ "$(num_multiple_range 89 3)" = "30" ]
    [ "$(num_multiple_range 90 3)" = "30" ]
    [ "$(num_multiple_range 91 3)" = "31" ]
}
