function incr_stack
{
    _stack_trace_+="/$@"
    log ""

}

function decr_stack
{
    _stack_trace_=$(dirname "$_stack_trace_")
    log ""

}

function log
{
    if [[ "${#_stack_trace_[@]}" -gt "30" ]]
    then
	echo -e "[${_stack_trace_:0:30}/../$(basename $stack_trace)] $@" >> "$_output_log_file_"
    else
	echo -e "[$_stack_trace_] $@" >> "$_output_log_file_"
    fi
    if [ "$_verbose_" -eq "1" ]
    then
	tail -n 1 "$_output_log_file_"
    fi
    
}

function show
{
    if [ "$_quiet_" -eq "1" ]
    then
	echo -e "$@" >> "$_output_log_file_"
    else
	echo -e "$@"
    fi
}

function incr
{
    #returns to stdout
    echo "$(( $1 + 1 ))"
}

function get_size
{
    echo "$(du -sBM "$@" | grep -o '^[0-9]*')"
}

function get_owner
{
    #$1: filepath
    echo "$(stat -c "%U:%G" "$1")"
}

function load_config_file
{
    incr_stack "load_config_file"
    local config_path="$1"
    if [[ -f "$config_path" ]]
    then
	log "loading config file"
	source "$config_path"
    else
	log "no config for $config_path"
    fi
    decr_stack
}

#void change_ownership(String path, String name)
function change_ownership
{
    incr_stack "change_ownership"

    path="$1"
    inbox="$2"
    name="$3"
    
    local owner_of_inbox="$(get_owner "$path/$inbox")"
    
    if [ -f "$path/$name" ]
    then
	log "changing ownership of $name"
	chown "$owner_of_inbox" "$path/$name"
    fi

    decr_stack
}
