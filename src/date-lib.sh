function inbox_YYYY
{
    echo "$2""-$(date -d "$1" +%Y)"
}

function inbox_MM
{
    echo "$2""-$(date -d "$1" +%Y-%m)"
}

function inbox_WW
{
    echo "$2""-$(date -d "$1" +%YW%W)"
}

function gen_archive_name
{
    incr_stack "gen_archive_name"
    local date_string="$1"
    local path="$2"
    local inbox="$3"

    log "inbox: $inbox"
    case $_archive_type_ in
	"Y")
	    local new_name="$(inbox_YYYY "$date_string" "$inbox")"
	    ;;
	"M")
	    local new_name="$(inbox_MM "$date_string" "$inbox")"
	    ;;
	"W")
	    local new_name="$(inbox_WW "$date_string" "$inbox")"
	    ;;
	*)
	    log "Invalid search criteria"
	    ;;
    esac

    local max=1900
    local part=1
    if [[ -f "$path/$new_name" ]]
    then
	if [[ "$(get_size "$path/$new_name")" -gt $max ]]
	then
	    local message="archive $new_name is greater than max allowed. Archiving into next part"
	    log $message
	    #show "\n"$message
	    local part=2
	    while [[ -f "$path/$new_name""p$part" ]] && [[ "$(get_size "$path/$new_name""p$part")" -gt $max ]]
	    do
		local message="archive $new_name""p$part is greater than max allowed. Archiving into next part"
		log $message
		#show "\n$message"
		part="$(incr $part)"
	    done
	fi
    fi
    if [[ $part -gt 1 ]]
    then
	new_name="$new_name""p$part"
    fi
    
    echo "$new_name"
    decr_stack
}

function find_oldest_email
{
    incr_stack "find_oldest_email"
    path=$1
    # select first two date strings (one will be from the mailer-daemon which is always there by default) (grep)
    # then delete all lines except second (sed) then remove the From xyz@abc.com section leaving you with the date.
    oldest_email_date_string=$(grep -P -o -m 2 "From .*" $path | sed '2q;d' | sed 's/From [^ ]*  //')

    oldest_email_date=$(date -d "$oldest_email_date_string 1 day" "+%Y-%m-%d")
    echo "$oldest_email_date"
    log "oldest_email_date: $oldest_email_date"
    log "exiting find_oldest_email"
    decr_stack
}

function get_search_date
{
    local date_string="$1"

    case "$_archive_type_" in
	"Y")
	    date_string="$(get_search_date_year "$date_string")"
	    ;;
	"M")
	    date_string="$(get_search_date_month "$date_string" "1")"
	    ;;
	"W")
	    date_string="$(get_search_date_week "$date_string")"
	    ;;
	*)
	    show "Invalid search criteria"
	    log "Invalid search criteria"
	    date_string="invalid search criteria" #will cause mailutil to bail
	    ;;
    esac
    echo "$date_string"
}

function get_search_date_year
{
    local date_to_mod="$1"

    local year="$(( $(date -d "$date_to_mod" "+%Y") + 1 ))"
    local date_string="$(date -d "$year-01-01" "+%Y-%m-%d")"

    echo "$date_string"
}

function get_search_date_month
{
    local date_to_mod="$1"
    local increment="$2"

    local date_string=( $(date -d "$date_to_mod" "+%Y %-m %d") )
    echo "$(date -d "${date_string[0]}-${date_string[1]}-01 + $increment month" "+%Y-%m-%d")"
}

function get_search_date_week
{
    local date_to_mod="$1"

    local date_string=$(date -d "$date_to_mod" "+%Y-%m-%d")
    local offset=$(( 8 - $(date -d "$date_string" "+%u")))
    echo $(date -d "$date_string +$offset  days" "+%Y-%m-%d")
}
