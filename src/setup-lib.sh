function setup_global_vars
{
    _output_log_file_="/tmp/automail" #used until all options have been loaded (any chance of being changed loaded) then swapped to what _log_file_ points to
    _log_file_="/var/log/automail"
    
    _stack_trace_="main"
    
    _archive_weekly_="2"       #TUESDAY
    _archive_monthly_="1"      #1ST DAY OF MONTH
    _archive_yearly_="1"

    _archive_="0"
    _archive_file_list_=( 'inbox' )
    _directory_="/mail"
    _domain_="0"
    _dry_run_="1"
    _max_="2000"
    _override_max_="0"
    _force_archive_="0"
    _archive_type_="M"        # incremental archive
    _quiet_="0"
    _user_="0"
    _verbose_="0"
    _skip_array_=()
    #_skip_=( "d:skip_domain" "u:domain/skip_user" )
}

function print_global_vars
{
    incr_stack "print_global_vars"
    log "_log_file_: $_log_file_"
    log "_output_log_file_: $_output_log_file_"
    log "_stack_trace_: $_stack_trace_"
    
    log "_archive_weekly_: $_archive_weekly_"
    log "_archive_monthly_: $_archive_monthly_"
    log "_archive_yearly_: $_archive_yearly_"

    log "_archive_file_list_: $_archive_file_list_"
    log "_directory_: $_directory_"
    log "_domain_: $_domain_"
    log "_max_: $_max_"
    log "_archive_: $_archive_"
    log "_override_max_: $_override_max_"
    log "_archive_type_: $_archive_type_"
    log "_force_archive_: $_force_archive_"
    log "_quiet_: $_archive_type_"
    log "_user_: $_user_"
    log "_verbose_: $_verbose_"
    log "_skip_array_: ${_skip_array_[@]}"
    log "_dry_run_: $_dry_run_"
    decr_stack
}

function load_defaults_config_file
{
    incr_stack "load_defaults_config_file"
    log "Loading Defaults config file"
    local path_to_config_file="$1"
    
    if [[ -n "$path_to_config_file" ]]
    then
	if [[ -f "$path_to_config_file" ]]
	then
	    source "$path_to_config_file"
	else
	    show "config file does not exist"
	fi
	
    else
	path_to_config_file="~/.automail"
	if [[ -f "$path_to_config_file" ]]
	then
	    source "$path_to_config_file"
	fi
    fi
    decr_stack
}

function set_flags
{
    incr_stack "set_flags"
    log "Setting Flags"
    local args=( $@ )
    local offset="0"
    local loaded_config="0"

    log "args: ${args[@]}"
    if [ "${#args[@]}" -eq '0' ] || [ "${args[0]}" = "--help" ] || [ "${args[0]}" = "-?" ]
    then
	show "Usage: automail.sh [options {-q | -v} {-u & -d}]"
	show ""
	show "Flags:"
	show "  -?  this help message"
	show "  -a  archive files"
	show "  -c  path_to_config_file"
	show "  -D  mail_directory"
	show "  -d  specific_domain_to_use"
	show "  -f  force archiving even if not oversized on schedule"
	show "  --help  this help message"
	show "  -l  log file location (includes file name, not just path)"
	show "  -m  override_config_or_default_max"
	show "  -q  quiet(suppress_output)"
	show "  -r  Real run"
	show "  --skip-users \"domain1/user1 domain2/user1 ...\""
	show "  --skip-domainss \"domain1 domain2 ...\""
	show "  -t  archive_type( [M]onthly, [W]eekly )"
	show "  -u  specific_user_to_archive"
	show "  -v  verbose"
	exit
    else
	for i in $(seq 0 ${#args[@]})
	do
	    log "argument: ${args[$i + $offset]}"
	    
	    case "${args[$(( $i + $offset ))]}" in
		-a)
		    _archive_="1"
		    log "_archive_: $_archive_"
		    ;;
		-c)
		    offset=$(incr $offset)
		    load_defaults_config_file "${args[$(( $i + $offset ))]}"
		    loaded_config="1"
		    log "config_file: ${_config_file_[@]}"
		    ;;
		-r)
		    _dry_run_="0"
		    log "_dry_run_: $_dry_run_"
		    ;;
		-l)
		    offset=$(incr $offset)
		    _log_file_="${args[$(( $i + $offset ))]}"
		    log "_log_file_: $_log_file_"
		    log "_output_log_file_: $_output_log_file_"
		    ;;
		-D)
		    offset=$(incr $offset)
		    _directory_="${args[$(( $i + $offset ))]}"
		    log "directory: $_directory_"
		    ;;
		-d)
		    offset=$(incr $offset)
		    _domain_="${args[$(( $i + $offset ))]}"
		    log "domain: $_domain_"
		    ;;
		-f)
		    _force_archive_="1"
		    log "_force_archive_: $_force_archive_"
		    ;;
		-m)
		    offset=$(incr $offset)
		    _override_max_="${args[$(( $i + $offset ))]}"
		    log "override_max: $_override_max_"
		    ;;
		-q)
		    _quiet_="1"
		    log "quiet: $_quiet_"
		    ;;
		--skip-users)
		    #while the first character of next input != "-" (which would
		    #indicate the next flag
		    #offset=$(incr $offset)
		    while [ 1 ]
		    do
			offset=$(incr $offset)
			if [ "${args[$(( $i + $offset ))]:0:1}" = "-" ] ||
			       [ -z "${args[$(( $i + $offset ))]}" ]
			then
			    break
			fi
			_skip_array_+=("u:${args[$(( $i + $offset ))]}")
		    done
		    offset=$(( $offset - 1 ))
		    log "_skip_array_: ${_skip_array_[@]}"
		    ;;
		--skip-domains)
		    #while the first character of next input != "-" (which would
		    #indicate the next flag
		    #offset=$(incr $offset)
		    while [ 1 ]
		    do
			offset=$(incr $offset)
			if [ "${args[$(( $i + $offset ))]:0:1}" = "-" ] ||
			       [ -z "${args[$(( $i + $offset ))]}" ]
			then
			    break
			fi
			_skip_array_+=("d:${args[$(( $i + $offset ))]}")
		    done
		    offset=$(( $offset - 1 ))
		    log "_skip_array_: ${_skip_array_[@]}"
		    ;;
		-t)
		    offset=$(incr $offset)
		    _archive_type_="${args[$(( $i + $offset ))]}"
		    log "archive_type: $_archive_type_"
		    ;;
		-u)
		    offset=$(incr $offset)
		    _user_="${args[$(( $i + $offset ))]}"
		    log "user: $_user_"
		    ;;
		-v)
		    _verbose_="1"
		    log "verbose: $_verbose_"
		    ;;
	    esac
	    if [ $(( $i + $offset )) -ge ${#args[@]} ]
	    then
		break
	    fi
	done
	if [[ "$loaded_config" -ne "1" ]]
	then
	    load_defaults_config_file
	fi
	log "done\n\n"
    fi
    
    validate_flags
    swap_log_files
    decr_stack
}

function swap_log_files
{
    incr_stack "swap_log_files"

    cat "$_output_log_file_" >> "$_log_file_"
    rm "$_output_log_file_"
    _output_log_file_="$_log_file_"

    decr_stack
}

function validate_flags
{
    incr_stack "validate_flags"
    log "Validating flags"

    if [ "$_quiet_" -ne "0" ] && [ "$_verbose_" -ne "0" ]
    then
	log "...failed\n"
	log "    You cannot specify quiet with verbose\n"
	show "    You cannot specify quiet with verbose\n"
	exit
    fi
    log "."
    
    if [ "$_user_" != "0" ] && [ "$_domain_" = "0" ]
    then
	log "...failed\n"
	log "    You cannot specify a user without a domain\n"
	show "    You cannot specify a user without a domain\n"
	exit
    fi

    save_defaults

    print_global_vars
    
    log ".done\n"
    decr_stack
}

function save_defaults
{
    #_max_, and _archive_type_ need to be backed up as overwritten in
    #code for loading domain_defaults
    _default_max_="$_max_"
    _default_archive_type_="$_archive_type_"
    _default_force_archive_="$_force_archive_"
    _default_archive_="$_archive_"
    _default_archive_file_list_=( ${_archive_file_list_[@]} )
}
