MAILUTIL=/usr/sbin/mailutil

function check_skip_array
{
    incr_stack "skip_user"

    local domain="$1"
    local user="$2"
    for skip in "${_skip_array_[@]}"
    do
    	log "skip: $skip"
	log "user: $user"
	log "domain: $domain"
	if [[ "${skip:0:2}" = "u:" ]]
	then
	    local tmp_domain=$(echo "${skip[@]:2}" | grep -o "^[^\/]*")
	    local tmp_user=$(echo "${skip[@]:2}" | grep -o -P "(?<=/)[^\/]*$")
	    log "tmp_user: $tmp_user"
	    log "tmp_domain: $tmp_domain"
	    if [[ "$tmp_domain" = "$domain" ]] && [[ "$tmp_user" = "$user" ]]
	    then
		echo "skip"
		break;
	    fi
	elif [[ "${skip:0:2}" = "d:" ]]
	then
	    if [[ "$domain" = "${skip[@]:2}" ]]
	    then
		echo "skip"
		break;
	    fi
	fi 
    done

    decr_stack
}

function iterate_users
{
    incr_stack "iterate_users"

    local domain_path="$1"
    log "domain_path: $domain_path"

    log "check if _user_ set"
    if [ "$_user_" = "0" ] || [ -z "$_user_" ]
    then
	log "_user_ = 0"
	local path="$domain_path/*"
    else
	log "_user_ = $_user_"
	local path="$domain_path/$_user_"
    fi

    log "path (more specific version of domain_path): ${path[@]}"

    log "iterate over each path in \$path"
    #unquoted variable so it expands
    for user_path in $path
    do	
	if [[ ! -d "$user_path" ]]
	then
	    log "$user_path is not a directory"
	    show "$user_path is not a directory"
	else
	    # checking whether to skip, then loading user specific config
	    log "user_path: $user_path"
		
	    local domain="$( basename "$( dirname "$user_path" )")"
	    local user="$( basename "$user_path")"
	    
	    log "domain: $domain"
	    log "user: $user"
	    
	    local skip="$(check_skip_array "$domain" "$user")"
	    # if cont is set to 1 by the skip check loop then
	    # skip this round of the user archive loop
	    if [[ "$skip" = "skip" ]]
	    then
		unset skip
		log "skipping $user"
		continue
	    fi

	    log "reloading defaults for archiving"
	    # reload _default_max_ and _default_archive_type_
	    _max_="$_domain_max_"
	    log "_max_: $_max_"
	    _archive_type_="$_domain_archive_type_"
	    log "_archive_type_: $_archive_type_"
	    _force_archive_="$_domain_force_archive_"
	    log "_force_archive_: $_force_archive_"
	    _archive_="$_domain_archive_"
	    log "_archive_: $_archive_"
	    _archive_file_list_=( ${_domain_archive_file_list_[@]} )
 	    log "_archive_file_list_: ${_archive_file_list_[@]}"
	    unset _skip_
	    log "unset _skip_ in case it was set"

	    # load user config file
	    load_config_file "$user_path/.config"

	    log "--Archive Flags--"
	    log "domain : $domain"
	    log "user : $user"
	    log "_archive_type_ : $_archive_type_"
	    log "_max_ : $_max_"
	    log "_override_max_ : $_override_max_"
	    log "_force_archive_: $_force_archive_"
	    log "_archive_: $_archive_"
 	    log "_archive_file_list_: ${_archive_file_list_[@]}"
	    log "_skip_ : $_skip_"
	    
	    for inbox in ${_archive_file_list_[@]}
	    do
		dispatch_mailbox_for_checking "$user_path/mail/" "$inbox" "$user" "$domain"
	    done
	fi
    done
    log "done"
    decr_stack
}

function dispatch_mailbox_for_checking {
    incr_stack "dispatch_mailbox_for_checking"
    path="$1"
    inbox_orig="$2"
    user="$3"
    domain="$4"

    #Convert path to include any extra path placed in inbox
    #important for later functions
    local path="$path/$(dirname $inbox_orig)"
    local inbox="$(basename $inbox_orig)"

    if [[ ! -f "$path/$inbox" ]]
    then
	log "$path does not exist"
	show "$path does not exist"
    else
	if [[ "$_archive_" -eq "1" ]]
	then
	    show "\n $domain/$user: $inbox_orig"
	    if [[ "$_force_archive_" -eq "1" ]]
	    then
		show "checking if due"
		log "archive forced, checking if overdue"
		
		check_archive_due "$domain" "$user"
		archive_due="$?"
	    else
		log "archive not forced. Checking if greater than max"
		archive_due="0"
	    fi
	    
	    if [ "$archive_due" -eq "1" ]
	    then		    
		archive_inbox "$path" "$inbox"
	    else
		show "Checking greater than max"
		log "Checking if $domain/$user oversize"
		check_oversize "$path" "$inbox"
	    fi
	fi
    fi
    decr_stack
}

function iterate_domains
{
    incr_stack "iterate_domains"

    log "_directory_: $_directory_"
    log "checking if _domain_ specified"
    if [ "$_domain_" = "0" ] || [ -z "$_domain_" ]
    then
	log "_domain_ not specified"
	local path=( "$_directory_/*")
    else
	log "_domain_ specified: $_domain_"
	local path=("$_directory_/$_domain_")
    fi
    
    log "path: ${path[@]}"

    log "iterate over all domains in \$path"
    
    for domain_path in $path
    do
	#grab last section of domain dir path (i.e domain_path=a/b/c
	#selects only c)
	local domain="$(basename "$domain_path")"

	log "check in domain in _skip_array_"

	log "_skip_array_: ${_skip_array_[@]}"
	local skip="$(check_skip_array "$domain" "")"
	# if cont is set to 1 by the skip check loop then
	# skip this round of the user archive loop
	if [[ "$skip" = "skip" ]]
	then
	    unset skip
	    log "skipping $domain"
	    continue
	fi

	log "restoring archive default flags"
	# restore default max and archive_type
	_max_="$_default_max_"
	log "_max_: $_max_"
	_archive_type_="$_default_archive_type_"
	log "_archive_type_: $_archive_type_"
	_force_archive_="$_default_force_archive_"
	log "_force_archive_: $_force_archive_"
	_archive_="$_default_archive_"
	log "_archive_: $_archive_"
	_archive_file_list_=$_default_archive_file_list_
	log "_archive_file_list_: ${_archive_file_list_[@]}"

	if [ ! -f "$domain_path/.config" ]
	then
	    #skip this domain. Go to next domain
	    continue
	fi
	load_config_file "$domain_path/.config"

	log "--Archive Flags--"
	log "_max_: $_max_"
	log "_archive_type_: $_archive_type_"
	log "_force_archive_: $_force_archive_"
	log "_skip_: _skip_"
	log "_archive_file_list_: ${_archive_file_list_[@]}"
	
	
	# if domain specifies skip then skip
	if [[ -n "$_skip_" ]]
	then
	    show "skipping $domain_path"
	    log "skipping $domain_path"
	    unset _skip_
	    continue
	fi

	log "--backing up domain archive defaults--"
	# back up domain defaults to be restored each user
	_domain_max_=$_max_
	log "_domain_max_: $_domain_max_"
	_domain_archive_type_="$_archive_type_"
	log "_domain_archive_type_: $_domain_archive_type_"
	_domain_force_archive_="$_force_archive_"
	log "_domain_force_archive_: $_domain_force_archive_"
	_domain_archive_="$_archive_"
	log "_domain_archive_: $_domain_archive_"
	_domain_archive_file_list_=( ${_archive_file_list_[@]} )
 	log "_domain_archive_file_list_: ${_domain_archive_file_list_[@]}"

	log "--convert domain relative path to absolute path--"
	#convert relative path to absolute path. For some reason in the
	#script I can't just go ${path[@]:0:1}, it has to be 0, which is
	#strange as it works fine on cmdline
	if [ "$(echo "${domain_path:0:1}")" != "/" ]
	then
	    domain_path="$(pwd)/$domain_path"
	    log "domain_path $domain_path"
	fi
	
	iterate_users "$domain_path"
    done
    decr_stack
}

function check_archive_due
{
    incr_stack "check_archive_due"
    local domain="$1"
    local user="$2"

    log "--parameters--"
    log "domain: $domain"
    log "user: $user"
    log "_archive_type_: $_archive_type_"

    local archive_due=0

    log "--checking whether archive due based on _archive_type_--"
    case $_archive_type_ in
	"W")
	    # date +%u | day of week
	    if [ "$(date +%u)" -eq "$_archive_weekly_" ]
	    then
		log "$domain/$user is due for weekly archiving"
		show "$domain/$user is due for weekly archiving"

		local archive_due=1
	    else
		log "Not scheduled for archiving"
	    fi
	    ;;

	"M")
	    # date +%-d | day of month
	    if [ "$(date +%-d)" -eq "$_archive_monthly_" ]
	    then
		log "$domain/$user is due for monthly archiving"
		show "$domain/$user is due for monthly archiving"

		local archive_due=1
	    else
		log "Not scheduled for archiving"
	    fi
	    ;;
	"Y")
	    # date +%-j | day of year
	    if [ "$(date +%-j)" -eq "$_archive_yearly_" ]
	    then
		log "$domain/$user is due for monthly archiving"
		show "$domain/$user is due for monthly archiving"

		local archive_due=1
	    else
		log "Not scheduled for archiving"
	    fi
	    ;;
	*)
	    log "Not a valid search criteria"
	    show "Not a valid search criteria"
	    ;;
    esac
    decr_stack
    return "$archive_due"
}

function check_oversize
{
    incr_stack "check_oversize"

    local path="$1"
    local inbox="$2"
    log "checking whether $inbox oversize"
    local size="$(get_size "$path/$inbox")"
    local archive_oversize="0"

    log "size: $size"
    if [ ! -z "$_override_max_" ] && [ "$_override_max_" -ne "0" ]
    then
    	if [ "$size" -gt "$_override_max_" ]
    	then
    	    log "size > _override_max_"
    	    local archive_oversize="1"
	else
	    show "not oversize"
    	fi
    elif [ ! -z "$_max_" ] && [ "$_max_" -ne "0" ]
    then
	if [ "$size" -gt "$_max_" ]
	then
    	    log "size > Max"
    	    local archive_oversize="1"
	else
	    show "not oversize"
	fi
    fi

    if [ "$archive_oversize" -eq "1" ]
    then	
    	archive_inbox "$path" "$inbox"
    fi
    decr_stack
}

function prevent_archive_cur_time_period
{
    local search_date="$1"

    local skip_archive="0"
    local search_date_sec=$(date -d "$(date -d $search_date +%Y-%m-%d)" +%s)
    # TODO put into a seperate function
    if [ "$_archive_type_" = "W" ]
    then
	local current_week_part=$(date -d "- $(( $(date +%u) - 1 )) days" +%Y-%m-%d)
	local current_week_sec=$(date -d "$current_week_part" +%s)
	log "current_week part: $current_week_part"
	log "current_week_sec: $current_week_sec"
	log "search_date_sec: $search_date_sec"
	log "search_date: $search_date"
	if [ $search_date_sec -gt $current_week_sec ]
	then
	    local message="Error: $inbox is set to weekly, and trying to archive current week"
	    if [[ "$_dry_run_" -eq 1 ]]
	    then
		message="Dry Run: Error $inbox is set to weekly, and the period that would have been selected for archiving is within the current week"
	    fi
	    log $message
	    echo $message >&2
	    skip_archive=1
	fi
    else
	local current_month_part=$(date -d "- $(( $(date +%d) - 1 )) days" +%Y-%m-%d)
	local current_month_sec=$(date -d "$current_month_part" +%s)
	log "current_month part: $current_month_part"
	log "current_month_sec: $current_month_sec"
	log "search_date_sec: $search_date_sec"
	log "search_date: $search_date"
	if [ $search_date_sec -gt $current_month_sec ]
	then
	    local message="Error: $inbox is set to monthly, and trying to archive current month"
	    if [[ "$_dry_run_" -eq 1 ]]
	    then
		message="Dry Run: Error $inbox is set to monthly, and the period that would have been selected for archiving is within the current month"
	    fi
	    log $message
	    echo $message >&2
	    skip_archive=1
	fi
    fi
    echo "$skip_archive"
}


function archive_inbox
{
    incr_stack "archive_inbox"
    
    local path="$1"
    local inbox="$2"
    
    show "Archiving $inbox"
    local oldest_date="$(find_oldest_email "$path/$inbox")"

    local search_date="$(get_search_date "$oldest_date")"

    local skip_archive="$(prevent_archive_cur_time_period $search_date)"
   
    if [ $skip_archive -ne 1 ]
    then
	local name="$(gen_archive_name "$oldest_date" "$path" "$inbox")"

	log "oldest_date: $oldest_date"
	log "search_date: $search_date"
	log "name: $name"

	search_date="before $search_date"
	
	log "$MAILUTIL archive \"$path/$inbox\"\n\t \"$path/$name\" \"$search_date\""
	if [[ "$_archive_" -eq "1" ]] && [[ "$_dry_run_" -eq "0" ]]
	then
	    if [[ "$_quiet_" -eq "1" ]]
	    then
		$MAILUTIL archive "$path/$inbox" "$path/$name" "$search_date" 1> /dev/null 2> /tmp/mailutil-error-pipe.txt
	    else
		$MAILUTIL archive "$path/$inbox" "$path/$name" "$search_date" 2> /tmp/mailutil-error-pipe.txt
	    fi
	    log "mailutil exited with a status of $?"
	    #Mailutil always seems to exit with status of 1, regardless of
	    #errors. /tmp/mailutil-error-pipe.txt is a work around to
	    #detect to errors
	    if [ $(get_size /tmp/mailutil-error-pipe.txt) -gt 0 ]
	    then
		echo -e "Mailutil errored while archiving \"$path/$inbox\"\n\t \"$path/$name\" \"$search_date\"\nError Message: $(</tmp/mailutil-error-pipe.txt)"
		rm /tmp/mailutil-error-pipe.txt
		exit
	    fi

	    change_ownership "$path" "$inbox" "$name"
	    check_oversize "$path" "$inbox"
	else
	    if [[ "$_dry_run_" -eq "1" ]]
	    then
		log "would have run\n\t$MAILUTIL archive \"$path/$inbox\" \"$path/$name\" \"$search_date\""
		show "would have run\n\t$MAILUTIL archive \"$path/$inbox\" \"$path/$name\" \"$search_date\""
	    fi
	fi
    fi
    decr_stack
}
